# OS
import os
import urllib
import tarfile
import pickle
# Numpy
import numpy as np
# Pytorch
from torch.utils.data import Dataset


class CIFAR10(Dataset):
    """ Download and create dataset for training, validation, and test.
    Args:
        root (string): root directory of dataset download.
        train_type (string): which data set to create: 'train', 'val', or 'test'.
        transform (object): torchvision transforms object.
    """

    base_folder = 'cifar-10-batches-py'
    train_list = ['data_batch_1', 'data_batch_2', 'data_batch_3']
    val_list = ['data_batch_4', 'data_batch_5']
    test_list = ['test_batch']
    meta_file = 'batches.meta'

    def __init__(self, root, train_type='train', transform=None):
        super(CIFAR10, self).__init__()

        self.path = os.path.join(root, self.base_folder)
        self.transform = transform

        if not os.path.exists(root):
            self.download(root)

        if os.path.exists(self.path):
            self.data, self.targets = self.make_dataset(train_type)
            self.index2class = self.load_meta(self.meta_file)

    def download(self, root):
        os.mkdir(root)
        filename = os.path.join(root, 'cifar-10-python.tar.gz')
        url = 'https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz'

        # Download file from given url.
        urllib.request.urlretrieve(url, filename)

        # Extract files from tar.gz file.
        with tarfile.open(filename, 'r:gz') as tar:
            tar.extractall(path=root)

    def load_meta(self, metafile):
        with open(os.path.join(self.path, metafile), 'rb') as f:
            dicts = pickle.load(f, encoding='latin1')
            labels = dicts['label_names']
            return {i: c for i, c in enumerate(labels)}

    def make_dataset(self, train_type):
        data = []
        targets = []

        if train_type == 'train':
            data_list = self.train_list
        if train_type == 'val':
            data_list = self.val_list
        if train_type == 'test':
            data_list = self.test_list

        for dl in data_list:
            with open(os.path.join(self.path, dl), 'rb') as f:
                dicts = pickle.load(f, encoding='latin1')
                data.append(dicts['data'])
                targets.extend(dicts['labels'])

        data = np.vstack(data).reshape(-1, 3, 32, 32)
        # Convert to H x W x C
        data = data.transpose((0, 2, 3, 1))

        targets = np.array(targets, dtype=np.int)

        return data, targets

    def __len__(self):
        return self.data.shape[0]

    def __getitem__(self, index):
        """
        Args:
            index (int): Index of dataset item.
        Return:
            tuple (img_data, target): Tensor and label index.
        """

        np_data = self.data[index]
        if self.transform is not None:
            tensor_data = self.transform(np_data)

        target = self.targets[index]

        return tensor_data, target
