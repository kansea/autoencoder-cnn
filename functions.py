# OS
import itertools
# numpy
import numpy as np
# matplotlib
import matplotlib.pyplot as plt
# pillow
from PIL import Image
# Pytorch
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.transforms.functional as tf


# Setup image data mean and std
# MEAN = [0.5, 0.5, 0.5]
# STD = [0.5, 0.5, 0.5]
MEAN = [0.4914, 0.4822, 0.4465]
STD = [0.2023, 0.1994, 0.2010]


class ACNN(nn.Module):
    """ Autoencoder-Classificator model
        The model includes three module: 1) encoding; 2) decoding; 3) classification.
        Autoencoder is consist of convolution architecture.
        Classification is consist of MLP.
        args:
            num_class (int): output size of classification.
        return:
            en_x (tensor): output of encoding module.
            de_x (tensor): output of decoding module.
            y    (tensor): output of classification module.
    """

    def __init__(self, num_class=10):
        super(ACNN, self).__init__()

        # Encoding module
        self.encoder = nn.Sequential(
            nn.Conv2d(3, 6, kernel_size=3),
            nn.BatchNorm2d(6),
            nn.ReLU(inplace=True),

            nn.Conv2d(6, 16, kernel_size=3),
            nn.BatchNorm2d(16),
            nn.ReLU(inplace=True),
        )

        # Decoding module
        self.decoder = nn.Sequential(
            nn.ConvTranspose2d(16, 6, kernel_size=3),
            nn.BatchNorm2d(6),
            nn.ReLU(inplace=True),

            nn.ConvTranspose2d(6, 3, kernel_size=3),
            nn.BatchNorm2d(3),
            nn.Sigmoid()
        )

        # Classification module
        self.Conv = nn.Sequential(
            nn.Conv2d(16, 96, kernel_size=3),
            nn.BatchNorm2d(96),
            nn.ReLU(inplace=True),
            # nn.Dropout2d(p=0.2, inplace=True),

            nn.Conv2d(96, 96, kernel_size=3),
            nn.BatchNorm2d(96),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(2, 2),
            # nn.Dropout2d(p=0.2, inplace=True),
        )

        self.fc = nn.Sequential(
            nn.Linear(13824, 2048),
            nn.ReLU(inplace=True),
            nn.Linear(2048, 2048),
            nn.ReLU(inplace=True),
            nn.Linear(2048, num_class)
        )

    def forward(self, x):
        # Output of encoding
        en_x = self.encoder(x)

        # Output of classification
        y = self.Conv(en_x)
        y = self.fc(y.view(x.size(0), -1))

        # Output of decoding
        de_x = self.decoder(en_x)
        return en_x, de_x, y


def accuracy(output, target):
    """ Computes accuracy of classification module.
        args:
            output (tensor): output of model.
            target (tensor): labels tensor.
        return:
            preds (tensor): predicted label index.
            acc (float): value of accuracy.
    """

    # Top-1 predication
    _, preds = output.topk(1)
    preds = preds.squeeze(1)
    acc = preds.eq(target).float().sum().item() / output.shape[0]
    return preds, acc


def tensor2img(tensor):
    """ Converts normalized data to non-normalized data.
        args:
            tensor (tensor): 4D tensor data.
        return:
            imgs (tensor): tensor data, same shape with input tensor data.
    """

    imgs = np.zeros(tensor.shape)
    for i in range(imgs.shape[0]):
        imgs[i, :, :, :] = tf.normalize(tensor[i, :, :, :],
                                        [-m / s for m, s in zip(MEAN, STD)],
                                        [1 / s for s in STD]).cpu()
    return imgs


def train(logs, dataloader, model, criterions, optim, device, epoch):
    """ Training function
        args:
            logs (object): save loss, accuracy values to tensorboard.
            dataloader (list of tuple): list of tensor data.
            model (object): instance of network.
            criterions (dict of object): loss functions.
            optim (object): optimizer of network.
            device (str): string of cuda or cpu.
            epoch (int): index of epoch.
        return:
            losses (float): average loss of a batch.
            accuracies (float): average accuracy of a batch.
    """

    model.train()
    losses = 0
    l1s = 0
    l2s = 0
    accuracies = 0
    for i, data in enumerate(dataloader, 1):
        inputs, labels = data
        inputs = inputs.to(device=device)
        labels = labels.to(device=device)
        # Clears the gradients of all optimized tensors
        optim.zero_grad()
        encode, decode, output = model(inputs)
        # Computes loss
        l1 = criterions['encode'](decode, inputs)
        l2 = criterions['classificator'](output, labels)
        loss = (l1 + l2) / 2
        l1s += l1.item()
        l2s += l2.item()
        losses += loss.item()
        # Computes gradient of current tensor
        loss.backward()
        # Updates parameters
        optim.step()

        # Computes accuracy
        _, acc = accuracy(output, labels)
        accuracies += acc
        # Prints batch loss, accuracy and learn rate
        if i % 50 == 0:
            print('\tbatch: {0}\tloss: {1:.5f}\tacc: {2:.5f}\tlr: {3}'.format(
                i, losses / i, accuracies / i, optim.param_groups[0]['lr']))

    losses /= len(dataloader)
    accuracies /=  len(dataloader)

    print('\t---avg---\tloss: {0:.5f}\tacc: {1:.5f}'.format(
        losses, accuracies))

    # Save losses and accuracies log to tensorboard
    logs.add_scalars('Autoencode-Loss', {'train': l1s / len(dataloader)}, epoch)
    logs.add_scalars('Classificator-Loss', {'train': l2s / len(dataloader)}, epoch)
    logs.add_scalars('Classificator-Accuracy', {'train': accuracies}, epoch)

    return


def val(logs, dataloader, model, criterions, device, epoch):
    """ Validation function
        args:
            logs (object): save loss, accuracy values to tensorboard.
            dataloader (list of tuple): list of tensor data.
            model (object): instance of network.
            criterions (dict of object): loss functions.
            device (str): string of cuda or cpu.
            epoch (int): index of epoch.
        return:
            losses (float): average loss of a batch.
            accuracies (float): average accuracy of a batch.
    """

    model.eval()
    losses = 0
    l1s = 0
    l2s = 0
    accuracies = 0
    for i, data in enumerate(dataloader, 1):
        inputs, labels = data
        inputs = inputs.to(device=device)
        labels = labels.to(device=device)
        # Predication
        encode, decode, output = model(inputs)
        # Computes loss
        l1 = criterions['encode'](decode, inputs)
        l2 = criterions['classificator'](output, labels)
        loss = (l1 + l2) / 2
        l1s += l1.item()
        l2s += l2.item()
        losses += loss.item()

        # Computes accuracy
        _, acc = accuracy(output, labels)
        accuracies += acc

    losses /= len(dataloader)
    accuracies /= len(dataloader)

    print('\t---val---\tloss: {0:.5f}\tacc: {1:.5f}'.format(
        losses, accuracies))

    # Save losses and accuracies log to tensorboard
    logs.add_scalars('Autoencode-Loss', {'validation': l1s / len(dataloader)}, epoch)
    logs.add_scalars('Classificator-Loss', {'validation': l2s / len(dataloader)}, epoch)
    logs.add_scalars('Classificator-Accuracy', {'validation': accuracies}, epoch)

    return losses, accuracies


def test(logs, dataloader, model, label_names, device):
    """ Test function
        args:
            logs (object): save loss, accuracy values to tensorboard.
            dataloader (list of tuple): list of tensor data.
            model (object): instance of network.
            device (str): string of cuda or cpu.
    """

    model.eval()
    accuracies = 0
    # Confuse matrix
    mat = np.zeros((10, 10), np.int32)
    with torch.no_grad():
        for i, data in enumerate(dataloader, 1):
            inputs, labels = data
            inputs = inputs.to(device=device)
            labels = labels.to(device=device)

            # Predication
            encode, decode, output = model(inputs)
            output = F.log_softmax(output, dim=1)

            # Computes accuracy
            pred_idx, acc = accuracy(output, labels)
            accuracies += acc

            # Add data to confuse matrix
            for j in range(0, pred_idx.size(0)):
                mat[labels[j].item(), pred_idx[j].item()] += 1

            # Random pickup 8 images
            start = np.random.randint(inputs.size(0) - 9)
            # Add reconstructed images to tensorboard logs
            logs.add_images('Reconstructed Images', decode[start : start + 8, :, :, :], i)
            # Predicted labels and original images
            imgs = tensor2img(inputs[start : start + 8, :, :, :])
            pred_idx = [label_names[p.item()] for p in pred_idx[start : start + 8]]
            for img, pred in zip(imgs, pred_idx):
                logs.add_image(pred, img, i)

        # Add confuse matrix image to tensorboard logs
        mat_img = plot_mat(mat, label_names)
        logs.add_image('Confuse Matrix', mat_img)

        print('\t--test--\tclassification acc: {0:.5f}'.format(accuracies / len(dataloader)))


def plot_mat(mat, labels):
    """ Plot figure by using float matrix (numpy array).
        args:
            mat (numpy array): confuse matrix data with float type.
            labels (list of string): list of label strings.
        return:
            img (numpy array): reload confuse matrix images.
    """

    categories = [v for k, v in labels.items()]
    # Normalizes matrix to [0 - 1]
    out = np.zeros(mat.shape, np.float)
    for i in range(mat.shape[0]):
        row = mat[i, :]
        denomintor = np.sum(row)
        out[i, :] = row / denomintor
    # Draws figure
    fig = plt.figure()
    ax = fig.add_subplot(111)
    cax = ax.matshow(out, cmap='Reds', interpolation='nearest')
    fig.colorbar(cax, fraction=0.02)
    w, h = out.shape
    threshold = out.max() / 2
    for i, j in itertools.product(range(w), range(h)):
        ax.text(j, i, format(out[i, j], '.2f'),
                horizontalalignment='center',
                verticalalignment='center',
                color='white' if out[i, j] > threshold else 'black',
                fontsize=9)
    plt.xticks(range(w), categories, rotation=90, fontsize=14)
    plt.yticks(range(h), categories, fontsize=14)
    ax.grid(False)
    plt.tight_layout()
    plt.savefig('figures/confuse_matrix.png', format='png')
    img = np.array(Image.open('figures/confuse_matrix.png')).transpose(2, 0, 1)
    return img
