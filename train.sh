#!/bin/bash

docker run \
    --gpus all \
    --privileged \
    --name lin_homework \
    --rm \
    -v $PWD:/code \
    homework:lin \
    python3 -B main.py -e 150

sudo chown -R $UID:$GID $PWD
