FROM pytorch/pytorch:1.2-cuda10.0-cudnn7-devel

RUN apt-get update && apt-get install -y --no-install-recommends \
        build-essential \
        cmake \
        git \
        ca-certificates && \
    rm -rf /var/lin/apt/lists/*

RUN pip install matplotlib \
                future \
                tensorboard

WORKDIR /code
