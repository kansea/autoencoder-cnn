# OS
import os
import random
import argparse
# Numpy
import numpy as np
# Pytorch
import torch
import torchvision.transforms as transforms
from torch import nn, optim
from torch.utils.tensorboard import SummaryWriter
import torch.nn.functional as F
# Customized module
from dataset import CIFAR10
import functions as funs


def main(args):
    # Set random seed for python, torch and torch.cuda
    SEED = 11
    np.random.seed(SEED)
    random.seed(SEED)
    torch.manual_seed(SEED)

    # Setup environment GPU or CPU
    if torch.cuda.is_available():
        torch.cuda.manual_seed(SEED)
        torch.cuda.manual_seed_all(SEED)
        device = torch.device('cuda:0')
    else:
        device = torch.device('cpu')

    # Preprocessing image data
    train_transform = transforms.Compose([
        transforms.ToPILImage(),
        transforms.RandomResizedCrop(32, scale=(0.08, 1.0), ratio=(0.75, 1.33)),
        transforms.RandomRotation([-10, 10]),
        transforms.RandomHorizontalFlip(p=0.5),
        transforms.ToTensor(),
        transforms.Normalize(funs.MEAN, funs.STD)])
    val_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(funs.MEAN, funs.STD)])

    print('--> preparing network ...')
    # Create a model
    model = funs.ACNN()

    # Save model to tensorboard graph
    with SummaryWriter(log_dir='logs/net') as logs:
        dummy_input = torch.rand(args.batch_size, 3, 32, 32)
        logs.add_graph(model, input_to_model=dummy_input)

    # Move model to GPU or CPU
    model = model.to(device=device)
    if torch.cuda.device_count() > 1:
        # Parallels data to mulit-GPU if GPU > 1
        model = nn.DataParallel(model, device_ids=None)

    # Setup loss functions for different task
    criterions = {'encode': nn.MSELoss().to(device=device),
                  'classificator': nn.CrossEntropyLoss().to(device=device)}

    # Setup optimizer to update weights
    optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=0.0001)
    # optimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=0.9, weight_decay=0.0001)

    # Reduce learning rate when metric has stopped improving
    scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', patience=10)

    # Load pretrained model if args.resume is not None
    if args.resume:
        print('--> loading checkpoint {}'.format(args.resume))
        checkpoint = torch.load(args.resume)
        start_epoch = checkpoint['epoch']
        model.load_state_dict(checkpoint['state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer'])

    if not args.test:
        print('--> preparing dataset ...')
        # 50% of whole dataset is train set
        train_data = CIFAR10(args.path, train_type='train', transform=train_transform)
        # 30% of whole dataset is validation set
        val_data = CIFAR10(args.path, train_type='val', transform=val_transform)
        # Create data loader to feed model by batch size
        train_loader = torch.utils.data.DataLoader(train_data, batch_size=args.batch_size,
                                                   shuffle=True, num_workers=args.jobs,
                                                   pin_memory=True)
        val_loader = torch.utils.data.DataLoader(val_data, batch_size=args.batch_size,
                                                 shuffle=False, num_workers=args.jobs,
                                                 pin_memory=True)
        print('--> beginning train ...')
        best_loss = 100
        # Setup tensorboard logs to save lass and accuracy values
        description = 'train-epoch-{}_batch-{}'.format(args.epochs, args.batch_size)
        with SummaryWriter(log_dir='logs/'+ description) as logs:
            for epoch in range(1, args.epochs + 1):
                print('Epoch: [ {} / {}]'.format(epoch, args.epochs))
                funs.train(logs, train_loader, model, criterions, optimizer, device, epoch)
                val_loss, val_acc = funs.val(logs, val_loader, model, criterions, device, epoch)
                scheduler.step(val_loss)
                # Save best model state
                if best_loss > val_loss:
                    best_acc = val_acc
                    states = {
                        'epoch': epoch,
                        'loss': best_loss,
                        'acc': val_acc,
                        'state_dict': model.state_dict(),
                        'optimizer': optimizer.state_dict()}
            # Save model
            torch.save(states, 'best_model.pth')
    else:
        print('--> beginning test')
        # 20% of whole dataset is test set
        test_data = CIFAR10(args.path, train_type='test', transform=val_transform)
        test_loader = torch.utils.data.DataLoader(test_data, batch_size=args.batch_size,
                                                  shuffle=False, num_workers=args.jobs,
                                                  pin_memory=True)
        # Setup tensorboard logs to save lass and accuracy values
        with SummaryWriter(log_dir='logs/test') as logs:
            funs.test(logs, test_loader, model, test_data.index2class, device)


if __name__ == '__main__':
    parse = argparse.ArgumentParser(description='Train Autoencoder with Classification')
    parse.add_argument('-p', '--path', type=str, default='.dataset',
                       help='Path to download dataset')
    parse.add_argument('-t', '--test', action='store_true', default=False,
                       help='Test only')
    parse.add_argument('-b', '--batch_size', type=int, default=128,
                       help='Mini batch size, default=128')
    parse.add_argument('-e', '--epochs', type=int, default=150,
                       help='Number of epoch, default=150')
    parse.add_argument('-l', '--lr', type=float, default=0.001,
                       help='Learning rate, default=0.001')
    parse.add_argument('-j', '--jobs', type=int, default=4,
                       help='Number of workers, default=4')
    parse.add_argument('-r', '--resume', type=str, default='',
                       help='Resume model path, default is None')
    args = parse.parse_args()

    main(args)
