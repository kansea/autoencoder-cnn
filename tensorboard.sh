#!/bin/bash

docker run \
    --gpus all \
    --privileged \
    --name lin_homework \
    --rm \
    -p 0.0.0.0:6006:6006 \
    -v $PWD:/code \
    homework:lin \
    tensorboard --logdir=logs --bind_all
