## Autoencoder with Classificator  
This project demonstrated how to train Autoencoder and Classificator together  
by using PyTorch.  
[**Here is a detailed report of this project.**](https://gitlab.com/kansea/autoencoder-cnn/-/wikis/Example-of-training-fused-network-(supervised-and-unsupervised))

#### Implementation environment:
* Ubuntu 18.04 with cuda 10.1
* Docker version >= 19.03.5
* Docker API version == 1.40
* Nvidia-docker version == 2.2.2

#### Download CIFAR-10 dataset:
The dataset will be download automatically to "*$PWD/.dataset*" when you frist  
to execute `train.sh` script.

#### Execution:
> You can change execution parameters in `*.sh` script, each script creates  
> docker container to run source code when you executed by `bash` command.  
> The created docker container will be removed automatically after execution.  
> Input your sudo password if requiried on training and test tasks.

1. Builds a docker image via `docker build -t homework:lin .`.
2. Begins network training via `bash train.sh`.
3. Tests network with test data set via `bash test.sh`.
4. Presents results of training, validation, and test via `bash tensorboard.sh`.
    - open web browser and input localhost:6006 to show tensorboard page.
